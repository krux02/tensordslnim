import moremacros, foldAst, tables, hashes

proc hash(arg: NimNode): Hash =
  case arg.kind
  of nnkSym, nnkIdent:
    result = hashIgnoreStyle($arg)
  of nnkIntLit, nnkInt8Lit, nnkInt16Lit, nnkInt32Lit, nnkInt64Lit, nnkUIntLit, nnkUInt8Lit, nnkUInt16Lit, nnkUInt32Lit, nnkUInt64Lit:
    result = hash(arg.intVal)
  of nnkFloatLit, nnkFloat32Lit, nnkFloat64Lit, nnkFloat128Lit:
    result = hash(int(arg.floatVal * 2e15)) # dirty hack
  of nnkStrLit, nnkRStrLit, nnkTripleStrLit:
    result = hash(arg.strVal)
  else:
    var h = hash(arg.kind)
    for child in arg.children:
      h = h !& hash(child)
    result = !$ h

proc isMultiplication(arg: NimNode): bool {.compileTime.} =
  arg.kind == nnkInfix and arg[0].repr == "*"

proc isAddition(arg: NimNode): bool {.compileTime.} =
  arg.kind == nnkInfix and arg[0].repr == "+"

proc isNegation(arg: NimNode): bool {.compileTime.} =
  arg.kind == nnkInfix and arg[0].repr == "-"

proc isLeafNode(arg: NimNode): bool =
  arg.len == 0

proc joinInfixToCall(arg: NimNode): NimNode {.compileTime.} =
  ## every time the is a nested multiplication, replace it with a varargs multiplication:
  if arg.isLeafNode or arg.kind == nnkBracketExpr:
    result = arg
  elif arg.isMultiplication:
    result = newCall(arg[0])
    var factors = newSeq[NimNode](0)

    proc collectFactors(node: NimNode): void =
      if node.isMultiplication:
        collectFactors(node[1])
        collectFactors(node[2])
      else:
        factors.add node

    collectFactors(arg)

    for factor in factors:
      result.add joinInfixToCall factor
  elif arg.isAddition or arg.isNegation:
    result = newCall(ident"+")

    var summands = newSeq[NimNode](0)

    proc collectSummands(summand: NimNode): void =
      if summand.isAddition:
        collectSummands(summand[1])
        collectSummands(summand[2])
      elif summand.isNegation:
        collectSummands(summand[1])
        var pos = summands.len
        collectSummands(summand[2])
        for i in pos .. high(summands):
          summands[i] = nnkInfix.newTree( ident"*", newLit(-1.0), summands[i] )
      else:
        summands.add summand

    collectSummands(arg)

    for summand in summands:
      result.add joinInfixToCall summand
  elif arg.kind == nnkAsgn:
    result = nnkAsgn.newTree(
      arg[0],
      joinInfixToCall(arg[1])
    )
  elif arg.kind == nnkStmtList:
    result = nnkStmtList.newTree
    for node in arg:
      result.add joinInfixToCall(node)
  elif arg.kind == nnkPar:
    result = joinInfixToCall(arg[0])
  else:
    error "incompatible tree for join Multiplications: " & arg.treerepr, arg
    result = arg

proc simplifyTypedAst(arg: NimNode): NimNode {.compileTime.} =
  ## removes all nodes that are hidden nodes from the typed ast
  if arg.isLeafNode:
    result = arg
  elif arg.kind == nnkHiddenAddr or arg.kind == nnkHiddenDeref:
    result = simplifyTypedAst(arg.head)
  elif arg.kind == nnkCall and $arg[0].symbol == "ta":
    arg[2].expectKind nnkHiddenStdConv
    arg[2][1].expectKind nnkBracket
    result = newBracketExpr(arg[1], [])
    for x in arg[2][1]:
      result.add x
  else:
    result = arg.kind.newTree
    for x in arg:
      result.add simplifyTypedAst(x)

import tables

macro tensordslInner*(debug:static[bool]; exportAssignment: static[openArray[bool]]; ast: typed): untyped =
  if debug:
    echo "in tensordslInner"

  let assignments =
    joinInfixToCall simplifyTypedAst(
      if ast.kind == nnkStmtList:
        ast
      else:
        newStmtList(ast)
    )

  if assignments.len != exportAssignment.len:
    var msg = "assignments and exportAssignment must be equal length:\n"
    msg &= $assignments.len
    msg &= " "
    msg &= $exportAssignment.len
    error msg

  if debug:
    for i in 0 ..< assignments.len:
      if exportAssignment[i]:
        echo "<export>"
        echo assignments[i].repr
        echo "</export>"

  var foldedAssignments : seq[NimNode] = foldAst(assignments)



  var exportedAssignments = newSeq[NimNode]()
  for i, exprot in exportAssignment:
    if exprot:
      exportedAssignments.add foldedAssignments[i]

  # table to assign each subexpression from the AST a symbol
  # when an expression is used more than onece in the
  # syntax tree, then it will only have one symbol assigned to it, and
  # therefore it will only be evaluated once.
  var expressionTable = newOrderedTable[NimNode, NimNode]()

  var symCounter = 0

  var numCalls = 0
  let finalAssignments = newStmtList()

  proc collectCalls(arg: NimNode): NimNode =
    # TODO this part needs to be part of AstFoldingStructure. The
    # AstFoldingstructure knows the dimensions of each subexpression,
    # and therfore it can handle to create the var section and the
    # assignment idividually

    if arg.len == 0:
      ## arg is literal or symbol
      return arg

    if expressionTable.hasKey arg:
      result = expressionTable[arg]
    else:
      result = genSym(nskVar, "expr_" & $symCounter & "_")
      symCounter += 1
      expressionTable[arg] = result # make sure table keys entries are never modified

      defer:
        finalAssignments.add newVarStmt(result, arg)

      if arg.kind == nnkCall and arg[0].eqIdent("contract"):
        numCalls += 1

        arg[1] = collectCalls(arg[1])
        arg[2] = collectCalls(arg[2])

      elif arg.kind == nnkCall and arg[0].eqIdent("transpose"):
        numCalls += 1

        arg[1] = collectCalls(arg[1])
        if arg.len == 3:
          arg.add newLit(1.0)
        if arg.len == 4:
          arg.add newLit(0.0)

      elif arg.kind == nnkCall and arg[0].eqIdent("sum"):
        numCalls += 1
        for i in 1 ..< arg.len:
          arg[i] = collectCalls(arg[i])

      elif arg.kind == nnkCall and arg[0].eqIdent("sumTranspose"):
        numCalls += 1
        arg[1] = collectCalls(arg[1])

      else:
        echo "nomatch:\n", arg.repr

  for asgn in exportedAssignments:
    asgn.expectKind nnkAsgn
    let sym = collectCalls(asgn[1])
    assert finalAssignments[^1][0][0] == sym
    finalAssignments[^1] = nnkAsgn.newTree(asgn[0], finalAssignments[^1][0][^1])

  result = finalAssignments

  if debug:
    echo "numCalls: ", numCalls

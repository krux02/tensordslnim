import macros

## sequence operations ##

proc back*[T](data: seq[T]): T = data[high(data)]
proc back*[T](data: openarray[T]): T = data[high(data)]


proc head*[T](data: seq[T]): T = data[0]
proc head*[T](data: openarray[T]): T = data[0]

proc sorted*[T](data: seq[T]): seq[T] =
  data.sorted(cmp)

proc indices*[T](arg: seq[T]): auto =
  0 ..< len(arg)

proc mkString*[T](data : openarray[T]; before, sep, after: string) : string =
  result = before
  result &= $data[0]
  for i in 1 .. high(data):
    result &= sep
    result &= $data[i]
  result &= after

proc mkString*[T](data : openarray[T]; sep: string) : string =
  mkString(data, "", sep, "")

proc fillIota*(arg: var seq[SomeNumber]): void =
  for i, value in arg.mpairs:
    value = SomeNumber(i)

########################
# string interpolation #
########################

proc isIdentChar(c: char): bool =
  'A' <= c and c <= 'Z' or 'a' <= c and c <= 'z' or '0' <= c and c <= '9' or c == '_'

macro s*(arg: static[string]): string =
  # does not handle utf8 runes properly
  # pretents everything is ASCII
  # no way to escape the $ sign. Should probably be $$.
  result = nnkStmtListExpr.newTree()
  let str = genSym(nskVar, "str")
  result.add(quote do:
    var `str`: string = ""
  )

  var i = 0
  var j = 0
  while true:
    while j < len(arg) and arg[j] != '$':
      j += 1

    let lit = newLit(arg[i..<j])
    result.add(quote do:
      `str`.add(`lit`)
    )

    if j == len(arg):
      break

    var exprString : string

    if arg[j+1] == '{':
      j += 2
      i = j
      while j < len(arg) and arg[j] != '}':
        if arg[j] == '{':
          error "{ not allowed here"
        j += 1

      exprString = arg[i..<j]

      j += 1
    else:
      j += 1
      i = j
      while j < len(arg) and arg[j].isIdentChar:
        # does not deal with the fact that identifiers may not start or end on _,
        # also does not deal with the fact that the first char may not be a digit
        j += 1
      exprString = arg[i..<j]

    let expr = parseExpr(exprString)
    result.add(quote do:
      `str`.add($`expr`) ## a
    )

    if j == len(arg):
      break

    i = j

  result.add str

## etc ##

proc joinValue*[T](a,b: T): T =
  ## assumes two arguments are the same, and just returns one of them
  ## fails when they are not the same
  assert(a == b)
  a

when isMainModule:
  let
    myName = "Arne"
    age    = 29
  let str = s"Hello, my name is $myName, I am $age years old. In five years I am ${age + 5}"
  doAssert str == "Hello, my name is Arne, I am 29 years old. In five years I am 34"

##########
# random #
##########

import mersenne, math

var mt = newMersenneTwister(seed = 123)

proc rand_u64*(): uint64 =
  (uint64(mt.getNum) shl 32) or uint64(mt.getNum)

proc rand_u32*(): uint32 = mt.getNum

proc rand_i32*(): int32 = rand_u32().int32

proc rand_i64*(): int64 = rand_u64().int64

proc rand_u16*(): uint16 = mt.getNum.uint16

proc rand_i16*(): int16 = mt.getNum.int16

proc rand_u8*(): uint8   = mt.getNum.uint8

proc rand_i8*(): int8   = mt.getNum.int8

proc rand_f64*(): float64 =
  rand_u64().float64 / pow(2'f64, 64'f64)

proc rand_f32*(): float32 =
  rand_u32().float32 / pow(2'f32, 32'f32)

proc rand*(maxval: uint64): uint64 =
  let limit = not(0'u64) - not(0'u64) mod maxval
  var bits = rand_u64()
  while bits > limit:
    bits = rand_u64()
  result = bits mod maxval

proc rand*(maxval: uint32): uint32 =
  let limit = uint32(high(uint32)) - uint32(high(uint32)) mod maxval
  var bits = rand_u32()
  while bits > limit:
    bits = rand_u32()
  result = bits mod maxval

proc rand*(maxval: int32): int32 =
  assert(maxval > 0)
  rand(maxval.uint32).int32

proc rand*(maxval: int64): int64 =
  assert(maxval > 0)
  rand(maxval.uint64).int64

#######
# etc #
#######

macro namedEcho*(args: varargs[untyped]): untyped =
  result = newStmtList()
  for arg in args:
    let lit = newLit(arg.repr & ":\n")
    result.add newCall(bindSym"echo", lit, arg)

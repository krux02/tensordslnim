import macros


proc printExpr(str: var string; arg: NimNode): void =
  case arg.kind
  of nnkInfix:
    str.printExpr(arg[1])
    if arg[0].repr == "*":
      str &= r" \cdot "
    if arg[0].repr == ":=":
      str &= r" \coloneqq "
    else:
      str &= ' '
      str &= arg[0].repr
      str &= ' '
    str.printExpr(arg[2])
  of nnkPrefix:
    str &= arg[0].repr
    str.printExpr(arg[1])
  of nnkPar:
    str &= '('
    assert arg.len == 1
    str.printExpr(arg[0])
    str &= ')'
  of nnkFloat64Lit:
    str &= $arg.floatVal.float64
  of nnkExportStmt:
    str.printExpr(arg[0])
  of nnkIdent:
    str &= $arg
  of nnkBracketExpr:
    str &= arg[0].repr
    str &= "_{"
    for i in 1 ..< len(arg):
      if i != 1:
        str &= ", "
      str &= arg[i].repr
    str &= "}"
  else:
    error "unexpected node kind: " & $arg.kind, arg

proc exportLatex*(arg: NimNode): void =
  echo r"\begin{equation}"
  defer:
    echo r"\end{equation}"

  for stmt in arg:
    if stmt.kind == nnkIdent:
      continue
    var str = ""
    str.printExpr(stmt)
    echo str

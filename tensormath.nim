import macros, myutils, moremacros

type
  Dimensions* = object
    N*: int
    values*: array[16, int]

  Index = object
    ## in index for tensor types
    N*: int
    values*: array[16, int]

  Permutation* = object
    N*: int
    values*: array[16, int]

  Contraction* = object
    N*: int
    values*: array[16, array[2,int]]

proc add*(contraction: var Contraction; a,b: int): void =
  let n = contraction.N
  contraction.N += 1
  contraction.values[n] = [a,b]

template CollectionMethods(Type,ElementType: untyped): untyped =
  proc len*(coll: Type): int = coll.N

  proc high*(coll: Type): int = coll.N - 1

  proc indices*(coll: Type): auto = 0 .. coll.N - 1

  proc add*(coll: var Type; value: ElementType): void =
    let n = coll.N
    coll.N += 1
    coll.values[n] = value

  proc `len=`*(coll: var Type; n: int): void =
    coll.N = n

  proc `[]`*(coll: Type; i: int): ElementType =
    assert 0 <= i and i < coll.N
    coll.values[i]

  proc `[]=`*(coll: var Type; i: int; value: ElementType): void =
    assert 0 <= i and i < coll.N
    coll.values[i] = value

  iterator items*(coll: Type): ElementType =
    for i in 0 ..< coll.N:
      yield coll.values[i]

  iterator pairs*(coll: Type): (int, ElementType) =
    for i in 0 ..< coll.N:
      yield (i, coll.values[i])

  when ElementType is int:
    macro u32array(coll: static[Type]): untyped =
      result = nnkBracket.newTree()
      for x in coll:
        var lit = newNimNode(nnkUInt32Lit)
        lit.intVal = x
        result.add lit

CollectionMethods(Dimensions, int)
CollectionMethods(Index, int)
CollectionMethods(Permutation, int)
CollectionMethods(Contraction, array[2,int])

proc hasLeftIndex(arg: Contraction, index: int): bool =
  for i in 0 ..< arg.N:
    if arg.values[i][0] == index:
      return true
  return false

proc hasRightIndex(arg: Contraction, index: int): bool =
  for i in 0 ..< arg.N:
    if arg.values[i][1] == index:
      return true
  return false

proc cumProd*(arg: Dimensions): Dimensions =
  result.values[0] = 1
  result.N = arg.N
  for i in 1 ..< result.N:
    result.values[i] = result.values[i-1] * arg.values[i-1]

proc dot(arg1,arg2: Dimensions) : int =
  let N = joinValue(arg1.N, arg2.N)
  for i in 0 ..< N:
    result += arg1.values[i] * arg2.values[i]

proc `$`*(v: Dimensions): string =
  v.values[0 ..< v.N].mkstring("dimensions(", ", ", ")")

proc `$`*(v: Permutation): string =
  v.values[0 ..< v.N].mkstring("permutation(", ", ", ")")

proc `$`*[T](v: openarray[T]): string =
  v.mkString("[", ", ", "]")

proc `$`*(v: Contraction): string =
  let tmp = v.values[0 ..< v.N]
  tmp.mkString("contraction(", ", ", ")")

proc `$`*(v: Index): string =
  v.values[0 ..< v.N].mkString("index(", ", ", ")")

proc product(dimensions: Dimensions) : int =
  result = 1
  for i in 0 ..< dimensions.N:
    result *= dimensions.values[i]

iterator indices(dims: Dimensions) : Index =
  var it : Index
  it.N = dims.N
  for _ in 0 ..< dims.product:
    yield it
    it.values[0] += 1
    for i in 1 ..< dims.N:
      if it.values[i-1] == dims.values[i-1]:
        it.values[i-1] = 0
        it.values[i] += 1
      else:
        break

proc dimensions*(args: varargs[int]): Dimensions =
  result.N = args.len
  for i in 0 .. high(args):
    result.values[i] = args[i]

proc newLit*(coll: Dimensions): NimNode {.compileTime.} =
  ## the resulting ast node should be equivalent as if a literal with the same value as the argument is used
  result = newCall(bindSym"dimensions")
  for x in coll:
    result.add newLit(x)

proc index(args: varargs[int]): Index =
  result.N = args.len
  for i in 0 .. high(args):
    result.values[i] = args[i]

proc contraction*(args: varargs[array[2,int]]): Contraction =
  result.N = args.len
  for i in 0 .. high(args):
    result.values[i] = args[i]

proc newLit*(coll: Contraction): NimNode {.compileTime.} =
  ## the resulting ast node should be equivalent as if a literal with the same value as the argument is used
  result = newCall(bindSym"contraction")
  for x in coll:
    result.add nnkBracket.newTree(newLit(x[0]), newLit(x[1]))

proc permutation*( args: varargs[int] ): Permutation {. compileTime .} =
  # check if args actually is a permutation

  for i in 0 .. high(args):
    let x = args[i]
    if x < 0 or high(args) < x:
      error("in args: " & args.mkString(" ") & s" arg x:$x is out of bounds 0 ≤ x < ${args.high}" )
      assert(false)

    for j in i+1 .. high(args):
      let y = args[j]
      if x == y:
        error("in args: " & args.mkString(" ") & s" args[$i] and args[$j] have equal value: $x")
        assert(false)

  result.N = len(args)
  for i in 0 .. high(args):
    result.values[i] = args[i]

proc newLit*(coll: Permutation): NimNode {. compileTime .} =
  ## the resulting ast node should be equivalent as if a literal with the same value as the argument is used
  result = newCall(bindSym"permutation")
  for x in coll:
    result.add newLit(x)

###########
# permute #
###########

template permuteIntern(dst, src, permutation: untyped): void =
  for i, idx in permutation:
    dst[i] = src[idx]

proc permute*[T](arg: seq[T]; permutation: openarray[SomeInteger]) : seq[T] =
  assert arg.len == permutation.len
  result.newSeq(arg.len)
  permuteIntern(result, arg, permutation)

proc permute*[T](arg: seq[T], permutation: Permutation) : seq[T] =
  assert arg.len == permutation.N
  result = newSeq[T](arg.len)
  permuteIntern(result, arg, permutation)

proc permute(arg: Index; permutation: Permutation): Dimensions =
  result.N = arg.N
  permuteIntern(result, arg, permutation)

proc permute(arg: Dimensions; permutation: Permutation): Dimensions =
  result.N = arg.N
  permuteIntern(result, arg, permutation)

template unpermuteIntern(dst, src: untyped; permutation: Permutation): void =
  for i, idx in permutation:
    dst[idx] = arg[i]

proc unpermute[T](arg: seq[T], permutation: Permutation) : seq[T] =
  assert arg.len == permutation.N
  result = newSeq[T](arg.len)
  unpermuteIntern(result, arg, permutation)

proc unpermute(arg: Index; permutation: Permutation): Dimensions =
  result.N = arg.N
  unpermuteIntern(result, arg, permutation)

proc unpermute(arg: Dimensions; permutation: Permutation): Dimensions =
  result.N = arg.N
  unpermuteIntern(result, arg, permutation)

type
  Scalar* = float64
  Tensor*[Dim : static[Dimensions]] = object
    data*: array[Dim.product, Scalar]

proc dataPtr*[Dim](arg: Tensor[Dim]): pointer = arg.data.unsafeAddr

proc init(tensor: Tensor) : void =
  ## does nothing for now
  discard

proc flat(dimensions: Dimensions, index: Index): int =
  let N = joinValue(dimensions.N, index.N)
  var factor = 1
  for i in 0 ..< N:
    let component = index.values[i]
    result += component * factor
    factor *= dimensions.values[i]

proc `[]`(tensor : Tensor; index : Index): float64 =
  tensor.data[tensor.Dimensions.flat(index)]

proc `[]=`*(tensor : var Tensor; indices : varargs[int], val: float64): void =
  assert indices.len == tensor.Dim.N
  tensor.data[tensor.Dim.flat(index(indices))] = val

proc `[]`*(tensor : Tensor; indices : varargs[int]): float64 =
  assert indices.len == tensor.Dim.N
  tensor.data[tensor.Dim.flat(index(indices))]

proc `[]`(this: Dimensions|Permutation; index: int): int =
  assert(0 <= index and index < this.N)
  this.values[index]

################################################################################
################################################################################
################################################################################

proc contractionDimensions(tta,ttb: Dimensions, contractionIndices: Contraction): Dimensions =
  for pairValue in contractionIndices:
    assert(tta.values[pairValue[0]] == ttb.values[pairValue[1]])

  var n = 0;
  for i, dim in tta:
    if not contractionIndices.hasLeftIndex(i):
      result.values[n] = dim
      n += 1

  for i, dim in ttb:
    if not contractionIndices.hasRightIndex(i):
      result.values[n] = dim
      n += 1

  result.N = n

macro tensorType*(args : varargs[int]): untyped =
  let call = newCall(bindSym"dimensions")
  for arg in args:
    call.add arg
  result = quote do:
    Tensor[`call`]

macro newTensor*(args: varargs[int]): untyped =
  let typeCall = newCall(bindSym"tensorType")
  for arg in args:
    typeCall.add arg
  let sym = genSym(nskVar, "tensor")
  let initCall = newCall(bindSym"init", sym)
  let identDef = newIdentDefs(sym,typeCall)
  let varSection = nnkVarSection.newTree(identDef)

  ## (var tensor : tensorType(args...); tensor.init; tensor)
  result = nnkStmtListExpr.newTree( varSection, initCall, sym )

proc dimensionsFromTensorSymbol(arg: NimNode): seq[int] {.compileTime.} =
  arg.expectKind nnkSym
  let typeInst = arg.getTypeInst
  if not typeInst[0].eqIdent("Tensor"):
    error("expected tensor type", arg)
  let N = typeInst[1][1][1].intVal.int
  result.newSeq(N)
  for i in 0 ..< N:
    result[i] = typeInst[1][2][1][i].intVal.int

macro contractionLoopCode(contractIndices: static[Contraction];
                          name0, name1, name2: typed) : untyped =
  let dims0 = dimensionsFromTensorSymbol(name0)
  let dimsA = dimensionsFromTensorSymbol(name1)
  let dimsB = dimensionsFromTensorSymbol(name2)

  type
    # T0(...) = T1(a,b,c,i) * T2(d,i,e)
    #                    ↑         ↑
    #   name=i         pos1=3    pos2=1

    ContractionIdentifier = tuple
      node: NimNode
      pos1, pos2, upper: int

    # T0(a,b,c) = T1(i,a,j) * T2(i,j,b,c)
    #    ↑             ↑
    #  pos0=0        pos1=1        name=a

    NonContractionIdentifierL = tuple
      node: NimNode
      pos0, pos1, upper: int

    # T0(a,b,c) = T1(i,a,j) * T2(i,j,b,c)
    #      ↑                         ↑
    #    pos0=1     name=b         pos2=2

    NonContractionIdentifierR = tuple
      node: NimNode
      pos0, pos2, upper: int

    # T0(a,b,c) = T1(i,a,j) * T2(i,j,b,c)
    #    ~ ~~~         ~             ~~~
    #                  1              2
    #  nonContracted1 = a
    #  nonContracted2 = b,c

  var
    nonContracted1 = newSeq[NonContractionIdentifierL](0)
    nonContracted2 = newSeq[NonContractionIdentifierR](0)
    contracted     = newSeq[ContractionIdentifier](0)

  for i, p in contractIndices:
    let lpos: int = p[0]
    let rpos: int = p[1]
    assert( dimsA[lpos] == dimsB[rpos] )

    var tmp = (node: genSym(nskForVar, myutils.s"i$i"),
               pos1: lpos, pos2: rpos, upper: dimsA[lpos])
    contracted.add(tmp)

    # T0(a,b) = T1(i,a,j) * T2(i,j,b)
    #    ~~~       ~~~~~       ~~~~~
    #     ↑          ↑           ↑
    #   args0      args1       args2

  var
    args0 = newSeq[NimNode](0)
    args1 = newSeq[NimNode](0)
    args2 = newSeq[NimNode](0)

  for i in 0 ..< len(dimsA):
    var j = 0
    while j < len(contracted) and contracted[j].pos1 != i:
      j += 1
    if j < len(contracted):
      args1.add contracted[j].node
    else:
      let upper: int = dimsA[i]
      let ident = (node: genSym(nskForVar, "a" & $i), pos0: args0.len, pos1: i, upper: upper)
      nonContracted1.add ident
      args0.add ident.node
      args1.add ident.node

  for i in 0 ..< len(dimsB):
    var j = 0;
    while j < len(contracted) and contracted[j].pos2 != i:
      j += 1
    if j < len(contracted):
      args2.add contracted[j].node
    else:
      let upper: int = dimsB[i]
      let ident = (node: genSym(nskForVar, "b" & $i), pos0: args0.len, pos2: i, upper: upper)
      nonContracted2.add ident
      args0.add ident.node
      args2.add ident.node


  ## s"${tensorAccess(name0, args0.toList)} =
  ##   ${tensorAccess(name1, args1.toList)} * ${tensorAccess(name2, args2.toList)}"

  var builder = newNimNodeBuilder()

  builder.blockBlock:
    ##val nonContracted = nonContracted1 ++ nonContracted2

    var nonContracted = newSeq[tuple[node:NimNode; upper:int]](0)

    for nc in nonContracted1:
      nonContracted.add( (node: nc.node, upper: nc.upper) )
    for nc in nonContracted2:
      nonContracted.add( (node: nc.node, upper: nc.upper) )

    for nc in nonContracted:
      let node = nnkInfix.newTree(ident"..<", newLit(0), newLit(nc.upper))
      builder.pushForStmt(nc.node, node)

    let accu = genSym(nskVar, "accu")
    builder.stmtList.add(quote do:
      var `accu` : float64 = 0
    )

    for nc in contracted:
      let node = nnkInfix.newTree(ident"..<", newLit(0), newLit(nc.upper))
      builder.pushForStmt(nc.node, node)

    let tensorAccess1 : NimNode = newBracketExpr(name1, args1)
    let tensorAccess2 : NimNode = newBracketExpr(name2, args2)

    builder.stmtList.add(quote do:
      `accu` += `tensorAccess1` * `tensorAccess2`
    )

    for nc in contracted:
      builder.pop

    let tensorAccess0 : NimNode = newBracketExpr(name0, args0)
    builder.stmtList.add(quote do:
      `tensorAccess0` = `accu`
    )
    for nc in nonContracted1:
      builder.pop
    for nc in nonContracted2:
      builder.pop



  result = builder.getResult

proc contract*[dimsA, dimsB](
    tensorA: Tensor[dimsA],
    tensorB: Tensor[dimsB],
    contractIndices: static[Contraction]):
      Tensor[contractionDimensions(dimsA, dimsB, contractIndices)] =

  contractionLoopCode(contractIndices, result, tensorA, tensorB)

proc contract*[dims](scalar: Scalar; tensor: Tensor[dims], contractIndices: static[Contraction]): Tensor[dims] =
  for i, x in tensor.data:
    result.data[i] = x * scalar

proc contract*[dims](tensor: Tensor[dims]; scalar: Scalar; contractIndices: static[Contraction]): Tensor[dims] =
  for i, x in tensor.data:
    result.data[i] = x * scalar

proc contract*(scalar1, scalar2: Scalar; contractIndices: static[Contraction]): Scalar =
  scalar1 * scalar2

## default
# alpha  = 1
# beta   = 0

proc transpose*[dims](tensor: Tensor[dims]; permutation: static[Permutation];
                      alpha: Scalar = 1; beta: Scalar = 0): Tensor[dims.permute(permutation)] =

  const cumProdPermDims = cumProd(result.Dim)
  var i = 0
  for decomposedIndex in tensor.Dim.indices:
    let
      permutedIndex = permute(decomposedIndex, permutation)
      j = dot(permutedIndex, cumProdPermDims)

    result.data[j] = tensor.data[i]
    i += 1

proc sum*[dims](args: varargs[Tensor[dims]]): Tensor[dims] =
  result.init
  for arg in args:
    for i,x in arg.data:
      result.data[i] += x

proc tensorTypeDimensions(tensor: NimNode): seq[int] =
  let typ = tensor.getTypeInst
  typ.expectKind nnkBracketExpr
  typ[0].expectKind nnkSym
  if not eqIdent(typ[0].repr, "Tensor"):
    error "expected type Tensor", tensor
  typ[1].expectKind nnkObjConstr
  typ[1][1].expectKind nnkExprColonExpr
  let N: int = typ[1][1][1].intVal.int
  result = newSeq[int](N)
  let bracket = typ[1][2][1]
  for i in 0 ..< N:
    result[i] = bracket[i].intVal.int

iterator reversePairs(arg: seq[NimNode]): tuple[key: int, val: NimNode] =
  var i = arg.high
  while i >= 0:
    yield(i, arg[i])
    i -= 1


macro sumTransposeTblisCode(dst, src: typed; permutations: static[openArray[Permutation]]): untyped =
  result = newStmtList()

  let dstDims = dst.tensorTypeDimensions
  let srcDims = src.tensorTypeDimensions

  if dstDims.len != srcDims.len:
    error "num dimensions mismatch dst: " & $dstDims & ", src: " & $srcDims, src

  let N = dstDims.len

  for i, perm in permutations:
    if N != perm.N:
      error("number of dimensions does not match in permutation at " & $i, src)

  # TODO implement this
  error("not implemented")


macro sumTransposeLoopCode(dst, src: typed; permutations: static[openArray[Permutation]]): untyped =
  result = newStmtList()

  let dstDims = dst.tensorTypeDimensions
  let srcDims = src.tensorTypeDimensions

  if dstDims.len != srcDims.len:
    error "num dimensions mismatch dst: " & $dstDims & ", src: " & $srcDims, src

  let N = dstDims.len

  for i, perm in permutations:
    if N != perm.N:
      error("number of dimensions does not match in permutation at " & $i, src)

  var innerBody = result

  var loopVars = newSeq[NimNode](N)
  for i in 0 ..< loopVars.len:
    loopVars[i] = genSym(nskForVar, "i" & $i & "_")

  for i, loopVar in loopVars.reversePairs:
    let limitLit = newLit(dstDims[i])

    innerBody.add(quote do:
      for `loopVar` in 0 ..< `limitLit`:
        `innerBody`
    )

    innerBody = innerBody[0][2]
    innerBody.expectKind nnkStmtList
    # get rid of the discard statement
    innerBody.del(0)
    innerBody.expectLen 0

  var sumExpr = newLit(0)

  for perm in permutations:
    let permLoopVars = loopVars.unpermute(perm)
    let bracketExpr = newBracketExpr(src, permLoopVars)
    sumExpr = nnkInfix.newTree(bindSym"+", sumExpr, bracketExpr)

  let lhs = newBracketExpr(dst, loopVars)
  innerBody.add(newAsgn(lhs, sumExpr))

proc sumTranspose*[dims](tensor: Tensor[dims];
                         permutations: static[openArray[Permutation]]): Tensor[dims] =
  sumTransposeLoopCode(result, tensor, permutations)

proc export_spacing(index: int, dimensions: Dimensions): int =
    var v = index
    for dim in dimensions.values:
      if v mod dim == dim - 1:
        result += 1
        v = v div dim
        continue
      break

proc `$`*[dims](tensor: Tensor[dims]): string =
  result = ""
  for i, x in tensor.data:
    result &= $x
    if i != high(tensor.data):
      let numNewLines = export_spacing(i, dims)
      if numNewLines > 0:
        for _ in 0 ..< numNewLines:
          result &= "\n"
      else:
        result &= ","

proc fillIota*[dims](arg: var Tensor[dims]): void =
  for i, x in arg.data.mpairs():
    x = Scalar(i)

proc fillFromArray*[dims, N, M](arg: var Tensor[dims]; data: array[N, array[M, Scalar]]): void =
  let numElements = dims.product
  assert data.len > 0
  assert numElements == data.len * data[0].len
  copyMem(arg.dataPtr, data.unsafeAddr, sizeof(Scalar) * numElements)

################################################################################
########################## tensor scalar operations ############################
################################################################################

template createOperatorTensorScalar( op: untyped ): untyped =
  proc `op`*[dims](tensor: Tensor[dims], scalar: float64): Tensor[dims] =
    for i, x in tensor.data:
      result.data[i] = `op`(x, scalar)

  proc `op`*(scalar: float64, tensor: Tensor): Tensor =
    for i, x in tensor.data:
      result.data[i] = `op`(scalar, x)

createOperatorTensorScalar(`*`)
createOperatorTensorScalar(`/`)
createOperatorTensorScalar(`+`)
createOperatorTensorScalar(`-`)

################
# export latex #
################

proc latex*[dims](tensor: Tensor[dims]): string =
  result = "$$\begin{bmatrix}"
  for i, x in tensor.data:
    result &= $x
    if i != high(tensor.data):
      let numNewLines = export_spacing(i, dims)
      if numNewLines > 0:
        for _ in 0 ..< numNewLines:
          result &= " \\\\\n"
      else:
        result &= " & "
  result.add "\end{bmatrix}$$"


include system/timers

type
  StopWatch* = object
    ## a stopwatch that uses the performance counter from sdl internally
    value : Ticks  # when running it's the offset, otherwise, it's the timer when it was stopped the last time
    running: bool

proc reset*(this: var StopWatch): int64 {. discardable .} =
  ## sets the timer to zero
  let counter = getTicks()

  result =
    if this.running:
      counter - this.value
    else:
      this.value - Ticks(0)


  this.value = counter

proc newStopWatch*(running: bool) : StopWatch =
  ## creates a new stop watch
  result.value   = getTicks()
  result.running = running

proc toggle*(this: var StopWatch): void {.inline.} =
  ## toggles the stop watch
  this.running = not this.running
  this.value = Ticks(getTicks().int64 - this.value.int64)

proc cont*(this: var StopWatch): void {.inline.} =
  ## continues the stopwatch when stopped
  if not this.running:
    this.toggle

proc stop*(this: var StopWatch): void {.inline.} =
  ## stops the stopwatch when running
  if this.running:
    this.toggle

proc timeNanoSeconds*(this: StopWatch): int64 {.inline.} =
  ## returns time in seconds

  if this.running:
    getTicks() - this.value
  else:
    this.value - Ticks(0)

proc timeSeconds*(this: StopWatch): float64 {.inline.} =
  float64(this.timeNanoSeconds()) / 1e9

proc running*(this: StopWatch): bool {.inline.} =
  ## tells weather this stopwatch is running
  this.running

import macros

proc heuristicTime*(arg: NimNode): float64 {.compileTime.} =
  echo "<heuristicTime>"
  defer:
    echo "</heuristicTime>"

  echo arg.repr

  return 0

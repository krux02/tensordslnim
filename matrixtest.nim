import math, myutils

###############################
# all vector and matrix types #
###############################

type
  ## w: width
  ## h: height
  ## s: stride
  ## Static: size is known at compile type
  ## Dynamic: size is stored in the header
  ## Sub: data is stored separate from the header
  ## static types without sub do only contain it's data and no header

  T = float64

  DataArray = UncheckedArray[T]

  StaticVector[h: static[int]] = object
    data*: array[h, T]

  DynamicVector = ptr object
    h: int
    data : DataArray

  StaticSubVector[h: static[int]] = object
    data : ptr array[h,T]

  DynamicSubVector = object
    h: int
    data : ptr DataArray

  StaticMatrix[w, h: static[int]] = object
    data*: array[w*h, T]

  DynamicMatrix = ptr object
    w,h: int
    data: DataArray

  DynamicSubMatrix = object
    w,h,s: int
    data : ptr DataArray

  StaticSubMatrix[w,h,s : static[int]] = object
    data : ptr array[(w-1)*s+h, T]

  SomeVector = StaticVector | DynamicVector | StaticSubVector | DynamicSubVector
  SomeMatrix = StaticMatrix | DynamicMatrix | StaticSubMatrix | DynamicSubMatrix

template s(mat: StaticMatrix): int = mat.h
template s(mat: DynamicMatrix): int = mat.h

#template T(t: typedesc[SomeMatrix]): typedesc = T
#template T(t: typedesc[SomeVector]): typedesc = T

########################
# dynamic constructors #
########################

echo sizeof(DynamicMatrix)

proc newDynamicMatrix(w,h: int): DynamicMatrix =
  let size = sizeof(DynamicMatrix) + sizeof(T) * w * h
  result = cast[DynamicMatrix](alloc0(size))
  result.w = w
  result.h = h

proc newDynamicVector(h: int): DynamicVector =
  let size = sizeof(DynamicVector) + sizeof(T) * h
  result = cast[DynamicVector](alloc0(size))
  result.h = h

proc delete(arg: var DynamicVector): void =
  dealloc(arg)
  arg = nil

proc delete(arg: var DynamicMatrix): void =
  dealloc(arg)
  arg = nil

#############
# iterators #
#############

iterator mitems(arg: var DynamicVector): var T =
  for i in 0 ..< arg.h:
    yield arg.data[i]

iterator mpairs(arg: var DynamicVector): tuple[key: int; value: var T] =
  for i in 0 ..< arg.h:
    yield(i, arg.data[i])

iterator mitems(arg: var DynamicMatrix): var T =
  for i in 0 ..< arg.w * arg.h:
    yield arg.data[i]

iterator mitems(arg: var StaticVector): var T =
  for x in arg.data.mitems():
    yield x

iterator mitems(arg: var StaticMatrix): var T =
  for x in arg.data.mitems():
    yield x


###########
# dataPtr #
###########

proc dataPtr(v: StaticVector) : ptr DataArray {.inline.} =
  cast[ptr DataArray](v.data.unsafeAddr)

proc dataPtr(v: DynamicVector) : ptr DataArray {.inline.} =
  v.data.unsafeAddr

proc dataPtr(v: StaticSubVector) : ptr DataArray {.inline.} =
  cast[ptr DataArray](v.data)

proc dataPtr(v: DynamicSubVector) : ptr DataArray {.inline.} =
  v.data

proc dataPtr(v: StaticMatrix) : ptr DataArray {.inline.} =
  cast[ptr DataArray](v.data.unsafeAddr)

proc dataPtr(v: DynamicMatrix) : ptr DataArray {.inline.} =
  v.data.unsafeAddr

proc dataPtr(v: StaticSubMatrix) : ptr DataArray {.inline.} =
  cast[ptr DataArray](v.data)

proc dataPtr(v: DynamicSubMatrix) : ptr DataArray {.inline.} =
  v.data

proc offset(data: ptr DataArray; off: int): ptr DataArray {.inline.} =
  cast[ptr DataArray](data[][off].addr)

###################
# converter procs #
###################

converter dynamicSubVector[h: static[int]](v: var StaticVector[h]) : StaticSubVector[h] =
  result.data = v.dataPtr

converter dynamicSubVector(vec: StaticVector) : DynamicSubVector =
  result.h = vec.h
  result.data = vec.dataPtr

converter dynamicSubVector(vec: DynamicVector) : DynamicSubVector =
  result.h = vec.h
  result.data = vec.dataPtr

converter dynamicSubVector(vec: StaticSubVector) : DynamicSubVector =
  result.h = vec.h
  result.data = vec.dataPtr

proc dynamicSubVector(vec: DynamicSubVector): DynamicSubVector =
  ## does nothing, return the input
  vec

converter dynamicSubMatrix(mat: var StaticMatrix) : DynamicSubMatrix =
  result.w = mat.w
  result.h = mat.h
  result.s = mat.s
  result.data = mat.dataPtr

converter dynamicSubMatrix(mat: StaticMatrix) : DynamicSubMatrix =
  result.w = mat.w
  result.h = mat.h
  result.s = mat.s
  result.data = mat.dataPtr

converter dynamicSubMatrix(mat: DynamicMatrix) : DynamicSubMatrix =
  result.w = mat.w
  result.h = mat.h
  result.s = mat.s
  result.data = mat.dataPtr

converter dynamicSubMatrix(mat: StaticSubMatrix) : DynamicSubMatrix =
  result.w = mat.w
  result.h = mat.h
  result.s = mat.s
  result.data = mat.dataPtr

proc dynamicSubMatrix(mat: DynamicSubMatrix): DynamicSubMatrix =
  ## does nothing, returns the input
  mat

########################
# matrix vector access #
########################

proc col[w,h: static[int]](mat: StaticMatrix[w,h]; col: int): StaticSubVector[h] {.inline.} =
  result.data = cast[ptr array[h, T]](mat.data[h * col].unsafeAddr)

proc col(mat: DynamicSubMatrix; col: int): DynamicSubVector {.inline.} =
  result.h = mat.h
  result.data = mat.data.offset(mat.s * col)

proc assign(dst,src: DynamicSubVector): void {.inline.} =
  assert dst.h == src.h
  copyMem(dst.dataPtr, src.dataPtr, sizeOf(T) * dst.h)

#proc setCol[w,h: static[int]](mat: var StaticMatrix; i: int; col : DynamicSubVector): void =
#  assign(mat.col(i), col)

proc `[]=`(arg: var DynamicMatrix; col,row: int; value: T): void {.inline.} =
  arg.data[col + row * arg.s] = value

##################
# fill test data #
##################

proc fillRandomNonUniform(mat: var StaticMatrix): void =
  for x in mat.mitems():
    x = rand_f64() * pow(10,rand_f64() * 10)

proc fillRandomUniform(mat: var StaticMatrix): void =
  for x in mat.mitems():
    x = rand_f64()

proc fillIota(mat: var StaticMatrix): void =
  for i in 0 ..< mat.w * mat.h:
    mat.data[i] = float64(i)

proc fillCoord(mat: var StaticMatrix): void =
  for x in 0 ..< mat.w:
    for y in 0 ..< mat.h:
      mat[y,x] = x + 0.01 * y


proc fillRandomNonUniform(mat: var DynamicMatrix): void =
  for x in mat.mitems():
    x = rand_f64() * pow(10,rand_f64() * 10)

proc fillRandomUniform(mat: var DynamicMatrix): void =
  for x in mat.mitems():
    x = rand_f64()

proc fillIota(mat: var DynamicMatrix): void =
  for i in 0 ..< mat.w * mat.h:
    mat.data[i] = float64(i)

proc fillCoord(mat: var DynamicMatrix): void =
  for x in 0 ..< mat.w:
    for y in 0 ..< mat.h:
      mat[y,x] = float64(x) + 0.01 * float64(y)





##########
# subMat #
##########

proc subMatrix(mat: SomeMatrix; x,y,w,h: int): DynamicSubMatrix =
  result.w = w
  result.h = h
  result.s = mat.s
  result.data = mat.dataPtr.offset(mat.s * x + y)

proc staticSubMatrix(mat: StaticMatrix | StaticSubMatrix; x,y,w,h: static[int]): auto =
  var res : matrixtest.StaticSubMatrix[w, h, mat.s]
  res.data = cast[ptr array[(w-1)*mat.s + h, T]](mat.dataPtr.offset(mat.s * x + y))
  return res

proc staticMatrix(mat: DynamicSubMatrix; w,h: static[int]): StaticMatrix[w,h] =
  assert w == mat.w
  assert h == mat.h
  for i in 0 ..< w:
    assign(dynamicSubVector(result.col(i)), mat.col(i))

#################
# string format #
#################

proc spaces(num: int): string =
  result = newString(num)
  for c in result.mitems:
    c = ' '

proc alignColumn*(data: ptr DataArray; length: int) : seq[string] =
  result = newSeq[string](length)
  for i in 0 ..< length:
    result[i] = $data[i]

  var lenLeft  = newSeq[int](length)
  var maxLenLeft = 0
  var lenRight = newSeq[int](length)
  var maxLenRight = 0

  for i, str in result:
    let index = str.find('.')
    let length = str.len
    lenLeft[i]  = index
    maxLenLeft = max(maxLenLeft, lenLeft[i])
    lenRight[i] = length - index - 1
    maxLenRight = max(maxLenRight, lenRight[i])

  for i, str in result.mpairs:
    str.insert(spaces(maxLenLeft  - lenLeft[i]))
    str.add(   spaces(maxLenRight - lenRight[i]))

proc alignColumn(vec: SomeVector): seq[string] =
  alignColumn(vec.dataPtr, vec.h)

proc `$`*(m: DynamicSubMatrix): string =
  var cols = newSeq[seq[string]](m.w)
  for i in 0 ..< m.w:
    cols[i] = alignColumn(m.col(i))

  result = ""
  for row in 0 ..< m.h:
    if row == 0:
      result &= "⎡"
    elif row == m.h - 1:
      result &= "⎣"
    else:
      result &= "⎢"

    for col in 0 ..< m.w:
      if col != 0:
        result &= "  "
      result &= cols[col][row]

    if row == 0:
      result &= "⎤\n"
    elif row == m.h - 1:
      result &= "⎦\n"
    else:
      result &= "⎥\n"

proc `$`*(mat: StaticMatrix): string =
  $ mat.subMatrix(0, 0, mat.w, mat.h)

proc `$`*(mat: StaticSubMatrix): string =
  $ mat.subMatrix(0, 0, mat.w, mat.h)

proc `$`*(mat: DynamicMatrix): string =
  $ mat.subMatrix(0, 0, mat.w, mat.h)

#########
# tests #
#########

when isMainModule:
  block:
    echo "static Matrix test"
    var mat : StaticMatrix[4,10]

    mat.fillRandomUniform


    echo mat

    let subMat = mat.subMatrix(1,2,3,6)
    let staticSubMat = mat.staticSubMatrix(1,2,3,6)
    let subMatCopy = staticMatrix(subMat, 3,6)

    echo "offset: 1 2"
    echo subMat
    echo staticSubMat
    echo subMatCopy

    let subSubMat = subMat.subMatrix(1,1,2, 2)
    let staticSubSubMat: StaticSubMatrix[2,2,10] = staticSubMat.staticSubMatrix(1,1,2,2)
    let subSubMatCopy = staticMatrix(subSubMat, 2, 2)

    echo "offset: 1 1"
    echo subSubMat
    echo staticSubSubMat
    echo subSubMatCopy

  block:
    echo "dynamic Matrix test"
    var mat = newDynamicMatrix(4, 10)

    mat.fillIota

    echo mat

    let subMat = mat.subMatrix(1,2, 3, 6)
    let subMatCopy = staticMatrix(subMat, 3,6)

    echo "offset: 1 2"
    echo subMat
    echo subMatCopy

    let subSubMat = subMat.subMatrix(1,1,2, 2)
    let subSubMatCopy = staticMatrix(subSubMat, 2, 2)

    echo "offset: 1 1"
    echo subSubMat
    echo subSubMatCopy

  ## TODO test Dynamic Matrix types
  ## TODO conversion between dynamic and static types
  ## TODO conversion between sub and nonsub types

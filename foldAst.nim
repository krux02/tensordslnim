import tensormath, sequtils, algorithm, macros, tables, hashes

type
  AstFoldingStructure = object
    ## used to be symbol, but NimSym is never equal
    indexVars: seq[string]
    dimensions: seq[int]
    node: NimNode

proc checkAst(arg: AstFoldingStructure): void =
  if arg.node.isNil:
    return
  let N = arg.indexVars.len
  assert N == arg.indexVars.len,  $N & " " & $arg.indexVars  & "node:\n" & $arg.indexVars  & " " & $arg.node.repr
  assert N == arg.dimensions.len, $N & " " & $arg.dimensions & "node:\n" & $arg.dimensions & " " & $arg.node.repr

proc sorted(arg: seq[string]): seq[string] =
  arg.sorted(cmp)

proc sameContent(seqA,seqB: seq[string]): bool =
  seqA.len == seqB.len and seqA.sorted == seqB.sorted

proc noDuplicates[T](arg: seq[T]): bool =
  let sortedArg = arg.sorted
  for i in 1 ..< sortedArg.len:
    if sortedArg[i-1] == sortedArg[i]:
      return false
  return true

proc transposeOrder(src,dst: seq[string]): Permutation {.compileTime.} =
  assert sameContent(src, dst)
  result.len = src.len
  for i in 0 ..< src.len:
    result[i] = src.find(dst[i])

proc productDims(args1, args2: seq[string]): Contraction {.compileTime.} =
  for i1, a1 in args1:
    let i2 = args2.find(a1)
    if i2 >= 0:
      result.add(i1,i2)

proc transpose(arg: AstFoldingStructure; target: seq[string]): AstFoldingStructure {.compileTime.} =
  defer:
    result.checkAst
  let perm = transposeOrder(arg.indexVars, target)
  result.indexVars = target
  result.dimensions = arg.dimensions.permute(perm)
  result.node = newCall(bindSym"transpose", arg.node, newLit(perm))

proc foldSum(args: openarray[AstFoldingStructure]): AstFoldingStructure =
  defer:
    result.checkAst
  result.indexVars  = args[0].indexVars
  result.dimensions = args[0].dimensions

  for arg in args:
    if not sameContent(result.indexVars, arg.indexVars):
      error $args[0].indexVars & " and " & $arg.indexVars & " is not compatible for +", arg.node

  # count symbol occurence:
  # special case for sumTranspose:

  ## TODO find proper solution to test, wether two Nodes are equivalent
  ## TODO detect more reliable cases for sumTranspose

  var isPureSumTranspose = true
  let repr0 = args[0].node.repr
  for i in 1 ..< args.len:
    let repri = args[i].node.repr
    if repri != repr0:
      isPureSumTranspose = false
      break

  if isPureSumTranspose:
    result.node = newCall(bindSym"sumTranspose", args[0].node, nnkBracket.newTree)
    for arg in args:
      let perm = transposeOrder(arg.indexVars, result.indexVars)
      result.node[2].add newLit(perm)
  else:
    result.node = newCall(bindSym"sum")
    for arg in args:
      if arg.indexVars == result.indexVars:
        result.node.add arg.node
      else:
        let perm = transposeOrder(arg.indexVars, result.indexVars)
        result.node.add newCall(bindSym"transpose", arg.node, newLit(perm))

proc mult(this: AstFoldingStructure, that: AstFoldingStructure): AstFoldingStructure {.compileTime.} =
  defer:
    result.checkAst

  assert( noDuplicates(this.indexVars), "this: " & $this.indexVars & " has duplicate elements")
  assert( noDuplicates(that.indexVars), "that: " & $that.indexVars & " his duplicate elements")

  let
    contraction    = productDims(this.indexVars, that.indexVars)
    contractionLit = newLit(contraction)

  ## symmetricDiff
  ## returns (arg1 ∪ arg2) \ (arg1 ∩ arg2)
  ## assumes each input sequence has each content value only once

  result.indexVars  = this.indexVars
  result.dimensions = this.dimensions
  for i in 0 ..< that.indexVars.len:
    let a = that.indexVars[i]
    let d = that.dimensions[i]
    let idx = result.indexVars.find(a)
    if idx >= 0:
      result.indexVars.delete(idx,idx)
      result.dimensions.delete(idx,idx)
    else:
      result.indexVars.add(a)
      result.dimensions.add(d)

  result.node      = newCall(bindSym"contract", this.node, that.node, contractionLit)

proc assign(lhs, rhs: AstFoldingStructure): AstFoldingStructure {.compileTime.} =
  assert(lhs.indexVars == rhs.indexVars)
  result.node = nnkAsgn.newTree(lhs.node, rhs.node)

proc foldAstIntern(node: NimNode): AstFoldingStructure {.compileTime.} =
  defer:
    result.checkAst
  case node.kind
  of nnkAsgn:
    node[0].expectKind nnkBracketExpr
    let lhs = foldAstIntern(node[0])
    let rhs = foldAstIntern(node[1])
    result = assign(lhs, transpose(rhs, lhs.indexVars))
  of nnkBracketExpr:
    result.node      = node[0]
    result.indexVars.newSeq(0)

    for i in 1 .. node.len-1:
      let sym = node[i].symbol
      result.indexVars.add(sym.repr)

    let typeInst = node[0].getTypeInst
    assert typeInst.kind == nnkBracketExpr
    assert $typeInst[0] == "Tensor", "expect tensor type"
    let objConstr = typeInst[1]
    objConstr.expectKind nnkObjConstr

    assert $objConstr[1][0] == "N"
    let N = objConstr[1][1].intVal.int

    assert $objConstr[2][0] == "values"
    let valuesBracket = objConstr[2][1]
    valuesBracket.expectKind nnkBracket

    result.dimensions.newSeq(N)

    for i in 0 ..< N:
      result.dimensions[i] = valuesBracket[i].intVal.int

  of nnkCall:
    if node[0].repr == "*":
      var factors = newSeqOfCap[AstFoldingStructure](node.len - 1)
      for i in 1 ..< node.len:
        factors.add foldAstIntern(node[i])

      ## TODO here add logic for best product order
      result = factors[0]
      for i in 1 ..< factors.len:
        result = mult(result, factors[i])

    elif node[0].repr == "+":
      var summands = newSeqOfCap[AstFoldingStructure](node.len - 1)
      for i in 1 ..< node.len:
        summands.add foldAstIntern(node[i])

      ## TODO here add logic for best sum order
      result = foldSum(summands)
    else:
      error "unknow call: " & node[0].repr & "\n" & node.repr

  of nnkFloat64Lit, nnkFloatLit:

    result.node = node
    result.indexVars.newSeq(0)
    result.dimensions.newSeq(0)

  else:
    error "unexpected node kind: " & $node.kind & "\n" & node.repr

proc hash(arg: NimNode): Hash =
  case arg.kind
  of nnkSym, nnkIdent:
    result = hashIgnoreStyle($arg)
  of nnkIntLit, nnkInt8Lit, nnkInt16Lit, nnkInt32Lit, nnkInt64Lit, nnkUIntLit, nnkUInt8Lit, nnkUInt16Lit, nnkUInt32Lit, nnkUInt64Lit:
    result = hash(arg.intVal)
  of nnkFloatLit, nnkFloat32Lit, nnkFloat64Lit, nnkFloat128Lit:
    result = hash(int(arg.floatVal * 2e15)) # dirty hack
  of nnkStrLit, nnkRStrLit, nnkTripleStrLit:
    result = hash(arg.strVal)
  else:
    var h = hash(arg.kind)
    for child in arg.children:
      h = h !& hash(child)
    result = !$ h

proc foldAst*(assignments: NimNode): seq[NimNode] =
  result.newSeq(0)
  assignments.expectKind nnkStmtList
  for asgn in assignments:
    if asgn.kind == nnkAsgn:
      result.add foldAstIntern(asgn).node
    else:
      error("not an assignment", asgn)

  var symbolMap = newTable[NimNode, NimNode]()
  for asgn in result:
    symbolMap[asgn[0]] = asgn[1]


  proc recSubstitudeSymbol(arg: NimNode): NimNode {.compileTime.} =
    ## substitude symbols with expression tree
    # TODO check for visited

    if arg.kind == nnkAsgn:
      arg[1] = recSubstitudeSymbol(arg[1])
      result = arg
    elif arg.kind == nnkSym:
      if symbolMap.hasKey(arg):
        result = symbolMap[arg]
      else:
        result = arg
    else:
      if arg.kind != nnkBracketExpr:
        for i in 0 ..< arg.len:
          arg[i] = recSubstitudeSymbol(arg[i])
        result = arg
      else:
        result = arg

  for i in 0 ..< len(result):
    result[i] = recSubstitudeSymbol(result[i])

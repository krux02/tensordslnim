import tensorDsl

var w : tensorType(2,2,2,2)
var r : tensorType(2,2,2,2)
var t : tensorType(2,2)
var tau : tensorType(2,2,2,2)
var T : tensorType(2,2,2,2)
var v : tensorType(2,2,2,2)
var x : tensorType(2,2,2,2)
var y : tensorType(2,2,2,2)
var u : tensorType(2,2,2,2)
var q : tensorType(2,2,2,2)

fillIota(w)
fillIota(r)
fillIota(t)
fillIota(tau)
fillIota(T)
fillIota(v)
fillIota(x)
fillIota(y)
fillIota(u)
fillIota(q)

tensorDsl:
  debug
  exportLatex

  W[b,m,j,e] := 2.0 * w[b,m,j,e] - x[b,m,e,j] + 2.0 * r[b,m,f,e] * t[f,j] -
                r[b,m,e,f] * t[f,j] - 2.0 * u[n,m,j,e] * t[b,n] -
                u[m,n,j,e] * t[b,n] + (2.0 * v[f,e,n,m] - v[f,e,m,n]) *
                (T[b,f,j,n] + 0.5 * T[b,f,n,j] - tau[b,f,n,j])

  X[b,m,e,j] := x[b,m,e,j] + r[b,m,e,f] * t[f,j] - u[m,n,j,e] * t[b,n] -
                v[f,e,m,n] * (tau[b,f,n,j] - 0.5 * T[b,f,n,j])

  U[m,n,i,e] := u[m,n,i,e] + v[f,e,m,n] * t[f,i]

  Q[m,n,i,j] := q[m,n,i,j] + u[m,n,i,e] * t[e,j] +
                u[n,m,j,e] * t[e,i] + v[e,f,m,n] * tau[e,f,i,j]

  P[j,i,m,b] := u[j,i,m,b] + r[b,m,e,f] * tau[e,f,i,j] +
                w[b,m,i,e] * t[e,j] + x[b,m,e,j] * t[e,i]

  H[m,e] := (2.0 * v[e,f,m,n] - v[e,f,n,m]) * t[f,n]

  F[a,e] := (-1.0 * H[m,e] * t[a,m]) + (2.0 * r[a,m,e,f] - r[a,m,f,e]) * t[f,m] -
            (2.0 * v[e,f,m,n] - v[e,f,n,m]) * T[a,f,m,n]

  G[m,i] := H[m,e] * t[e,i] + (2.0 * u[m,n,i,e] - u[n,m,i,e]) * t[e,n] +
            (2.0 * v[e,f,m,n] - v[e,f,n,m]) * T[e,f,i,n]

  export z[a,i] := -1.0 * G[m,i] * t[a,m] - (2.0 * U[m,n,i,e] -
                    U[n,m,i,e]) * T[a,e,m,n] + (2.0 * w[a,m,i,e] -
                    x[a,m,e,i]) * t[e,m] +
                    (2.0 * T[a,e,i,m] - T[a,e,m,i]) * H[m,e] +
                    (2.0 * r[a,m,e,f]  - r[a,m,f,e]) * tau[e,f,i,m]

  export Z[a,b,i,j] := v[a,b,i,j] + Q[m,n,i,j] * tau[a,b,m,n] +
                       y[a,b,e,f] * tau[e,f,i,j] + r[e,j,a,b] * t[e,i] -
                       P[i,j,m,b] * t[a,m] + F[a,e] * T[e,b,i,j] -
                       G[m,i] * T[a,b,m,j] + W[b,m,j,e] * T[a,e,i,m] -
                       0.5 * W[b,m,j,e] * T[a,e,m,i] - 0.5 * X[b,m,e,j] * T[a,e,m,i] -
                       X[b,m,e,i] * T[a,e,m,j] + r[e,i,b,a] * t[e,j] -
                       P[j,i,m,a] * t[b,m] + F[b,e] * T[e,a,j,i] -
                       G[m,j] * T[b,a,m,i] + W[a,m,i,e] * T[b,e,j,m] -
                       0.5 * W[a,m,i,e] * T[b,e,m,j] - 0.5 * X[a,m,e,i] * T[b,e,m,j] -
                       X[a,m,e,j] * T[b,e,m,i]

namedEcho z, Z

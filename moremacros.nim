import macros
import myutils

export macros

proc back*(node: NimNode): NimNode = node[node.len-1]
proc head*(node: NimNode): NimNode = node[0]

proc indices*(node: NimNode): auto =
  0 ..< len(node)


proc addAll*(dst, src: NimNode): NimNode {.discardable.} =
  for node in src:
    dst.add(node)
  dst

proc addAll*(dst: NimNode; src: openarray[NimNode]): NimNode {.discardable.} =
  for node in src:
    dst.add(node)
  dst

proc newIdentDefs*(names : openarray[NimNode], tpe,val: NimNode): NimNode =
  result = nnkIdentDefs.newTree(names)
  result.add tpe,val

proc newVarSection*(node: varargs[NimNode]): NimNode =
  result = nnkVarSection.newTree(node)

proc newAsgn*(lhs,rhs: NimNode): NimNode =
  nnkAsgn.newTree(lhs,rhs)

proc newBracketExpr*(node: NimNode, args: openarray[NimNode]): NimNode =
  result = nnkBracketExpr.newTree(node)
  result.addAll(args)

proc indexOf*(father,child: NimNode): int =
  for i in 0 ..< len(father):
    if father[i] == child:
      return i
  return -1


#####################
# more constructors #
#####################

proc newDotExpr*(a,b,c: NimNode): NimNode {.compileTime.} =
  newDotExpr(a,b).newDotExpr(c)

proc newBracketExpr*(a,b: NimNode): NimNode {.compileTime.} =
  nnkBracketExpr.newTree(a,b)

proc rangeUntil*(upper: int): NimNode {.compileTime.} =
  nnkInfix.newTree(ident"..<", newLit(0), newLit(upper))

proc newArrayLit*[T](elements: openarray[T]): NimNode {.compileTime.} =
  result = nnkBracket.newTree
  for element in elements:
    result.add newLit(element)

####################
# nim node builder #
####################

type
  NimNodeBuilder* = object
    data: seq[NimNode]
    # since the nim runtime doesn't handle pointer types, I need to update the
    # nimnode at the insertion point on pop
    insertionPoints: seq[seq[int]]

proc newNimNodeBuilder*(root: NimNode = newStmtList()): NimNodeBuilder =
  result.data = @[root]
  result.insertionPoints = @[]

proc getResult*(builder: NimNodeBuilder): NimNode =
  assert(builder.data.len == 1)
  builder.data[0]

proc stmtList*(builder: NimNodeBuilder): NimNode =
  builder.data.back

proc root*(builder: NimNodeBuilder): NimNode = builder.data[0]

proc get(dst: NimNode; index: seq[int]): NimNode =
  ## dst.get(@[1,2,3]) should be equivalent to dst[1][2][3]
  var it = dst
  for i in index:
    it = it[i]

  return it

proc pop*(builder: var NimNodeBuilder): NimNode {.discardable.} =
  let innerStmtList = builder.data.pop
  let index = builder.insertionPoints.pop
  let insertionPoint = builder.data.back.get(index)

  innerStmtList.expectKind nnkStmtList
  insertionPoint.expectKind nnkStmtList
  insertionPoint.del(insertionPoint.len)
  insertionPoint.addAll(innerStmtList)

proc pushBlockStmt*(builder: var NimNodeBuilder): void =
  builder.insertionPoints.add(@[builder.stmtList.len, 1])

  builder.data.back.add nnkBlockStmt.newTree(
    newEmptyNode(), newStmtList())

  builder.data.add(newStmtList())

proc pushForStmt*(builder: var NimNodeBuilder; loopVar, rangeVal: NimNode): void =
  builder.insertionPoints.add(@[builder.stmtList.len, 2])

  builder.data.back.add nnkForStmt.newTree(
    loopVar,
    rangeVal,
    newStmtList()
  )

  builder.data.add(newStmtList())

template blockBlock*(builder: var NimNodeBuilder, blk: untyped): void =
  builder.pushBlockStmt
  blk
  builder.pop

template forStmtBlock*(builder: var NimNodeBuilder; loopVar, rangeVal: NimNode, blk: untyped): NimNode =
  builder.pushForStmt
  blk
  builder.pop


#########################
# recursive ast collect #
#########################

type
  MatchProc* = proc(arg: NimNode): tuple[match: NimNode; stop: bool]
  MapProc*  = proc(x:NimNode): NimNode {.closure.}

proc recAstCollect(dst: var seq[NimNode]; arg: NimNode; fun: MatchProc): void =
  let (match, stop) = fun(arg)
  if not match.isNil:
    dst.add match
  if not stop:
    for node in arg:
      dst.recAstCollect(node, fun)

proc recAstCollect*(arg: NimNode; fun: MatchProc): seq[NimNode] =
  result.newSeq(0)
  result.recAstCollect(arg, fun)

proc recAstMap*(arg: NimNode; fun: MapProc): NimNode =
  ## like the functional operation map, just that it works on the recursive NimNode structure
  result = fun(arg)
  for i in 0 ..< len(result):
    result[i] = recAstMap(result[i], fun)

import tblis, macros, myutils

proc getDimsAndStrideArray(arg: NimNode): tuple[dims, stride: NimNode; ndims: int] =
  arg.expectKind nnkSym
  let bracketExpr = arg.getTypeInst
  bracketExpr.expectKind nnkBracketExpr
  if not bracketExpr[0].eqIdent "Tensor":
    error "expected Tensor type", arg
  let N = bracketExpr[1][1][1].intVal.int
  result.ndims = N
  result.dims = nnkBracket.newTree
  result.stride = nnkBracket.newTree
  let bracket = bracketExpr[1][2][1]
  var prod: int = 1
  for i in 0 ..< N:
    let intLit = bracket[i]
    let intVal = intLit.intVal.int
    result.dims.add intLit
    result.stride.add newLit(prod)
    prod *= intVal

proc getTblisTensorLit(arg: NimNode): tuple[ndims: int, sym, init: NimNode] =
  let (len_array, stride_array, ndims) = getDimsAndStrideArray(arg)
  result.ndims = ndims
  let ndimsLit = newLit(ndims)
  let tensorSym = genSym(nskVar, "tensor")
  result.sym = tensorSym
  result.init = quote do:
    var len = `len_array`
    var stride = `stride_array`
    var `tensorSym`: tblis.tensor
    `tensorSym`.kind = skDouble
    `tensorSym`.scalar.value_f64 = 1.0
    `tensorSym`.data = `arg`.dataPtr
    `tensorSym`.ndim = `ndimsLit`.int32
    `tensorSym`.len    = len[0].addr
    `tensorSym`.stride = stride[0].addr

proc newSeqWithValue(length: int, value: int): seq[int] =
  result.newSeq(length)
  for x in result.mitems:
    x = value

proc getTblisStrArgs(contr: Contraction; ndims_0, ndims_1, ndims_2: int): tuple[str0,str1,str2: string] {.compileTime.}=

  if ndims_0 != ndims_1 + ndims_2 - contr.len * 2:
    error($ndims_0 & " != " & $ndims_1 & " + " & $ndims_2 & " - " & $(contr.len * 2))

  var seq0 = newSeqWithValue(ndims_0, -1)
  var seq1 = newSeqWithValue(ndims_1, -1)
  var seq2 = newSeqWithValue(ndims_2, -1)

  var i = 0

  for pair in contr:
    seq1[pair[0]] = i
    seq2[pair[1]] = i
    i += 1

  var j = 0

  for k in 0 ..< seq1.len:
    if seq1[k] < 0:
      seq1[k] = i
      seq0[j] = i

      i += 1
      j += 1

  for k in 0 ..< seq2.len:
    if seq2[k] < 0:
      seq2[k] = i
      seq0[j] = i

      i += 1
      j += 1

  assert j == seq0.len

  result.str0 = ""
  for x in seq0:
    result.str0.add char(int('a') + int(x))
  result.str1 = ""
  for x in seq1:
    result.str1.add char(int('a') + int(x))
  result.str2 = ""
  for x in seq2:
    result.str2.add char(int('a') + int(x))

type
  UArray {.unchecked.} [T] = array[0, T]

proc uarray[T](arg: ptr T): ptr UArray[T] = cast[ptr UArray[T]](arg)

proc print_gen[T](arg: tblis.tensor) =
  let N = arg.len.uarray[arg.ndim-1] * arg.stride.uarray[arg.ndim-1]
  let data = cast[ptr UArray[T]](arg.data)
  for i in 0 ..< N:
    echo data[i]
    for j in 1 ..< arg.ndim:
      let stride = arg.stride.uarray[j]
      if i mod stride == stride-1:
        echo()

proc print(arg: tblis.tensor) =
  case arg.kind
  of skSingle:
    print_gen[float32](arg)
  of skDouble:
    print_gen[float64](arg)
  of skSComplex:
    print_gen[complex64](arg)
  of skDComplex:
    print_gen[complex128](arg)

macro contractionTblisCode*(contractionIndices: static[Contraction]; resultTensor, tensorA, tensorB: typed) : untyped =

  let (ndims_0, tblisResult, init0) = getTblisTensorLit(resultTensor)
  let (ndims_1, tblisA, init1) = getTblisTensorLit(tensorA)
  let (ndims_2, tblisB, init2) = getTblisTensorLit(tensorB)

  let (str0, str1, str2) = getTblisStrArgs(contractionIndices, ndims_0, ndims_1, ndims_2)

  result =  quote do:
    `init0`
    `init1`
    `init2`
    tblis.mult(nil, nil, `tblisA`.addr, `str1`, `tblisB`.addr, `str2`, `tblisResult`.addr, `str0`)

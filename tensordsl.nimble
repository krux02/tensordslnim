# Package

version       = "0.1.0"
author        = "Arne D\xC3\xB6ring"
description   = "domain language for tensor transformations"
license       = "MIT"

# Dependencies

requires "nim >= 0.16.1"


import dslmacros

tensorDsl:
  W[b,m,j,e] = (2*w[b,m,j,e] - x[b,m,e,j]) + (2*r[b,m,f,e] - r[b,m,e,f]) * t[f,j] - (2*u[n,m,j,e] - u[m,n,j,e])*t[b,n] +
    (2*v[f,e,n,m]-v[f,e,m,n]) * (T[b,f,j,n] + 0.5 * T[b,f,n,j] - tau[b,f,n,j])

  X[b,m,e,j] = x[b,m,e,j] + r[b,m,e,f] * t[f,j] - u[m,n,j,e] * t[b,n] - v[f,e,m,n] * (tau[b,f,n,j] - 0.5 * T[b,f,n,j])

  U[m,n,i,e] = u[m,n,i,e] + v[f,e,m,n] * t[f,i]

  Q[m,n,i,j] = q[m,n,i,j] + (1 + Perm[m/n, i/j]) * u[m,n,i,e] * t[e,j] + v[e,f,m,n] * tau[e,f,i,j]

  P[j,i,m,b] = u[j,i,m,b] + r[b,m,e,f] * tau[e,f,i,j] + w[b,m,i,e] * t[e,j] + x[b,m,e,j] * t[e,i]

  H[m,e] = (2 * v[e,f,m,n] - v[e,f,n,m]) * t[f,n]

  F[a,e] = -(H[m,e] * t[a,m]) + (2 * r[a,m,e,f] - r[a,m,f,e]) * t[f,m] - (2 * v[e,f,m,n] - v[e,f,n,m]) * T[a,f,m,n]

  G[m,i] = H[m,e] * t[e,i] + (2 * u[m,n,i,e] - u[n,m,i,e]) * t[e,n] + (2 * v[e,f,m,n] - v[e,f,n,m]) * T[e,f,i,n]

  z[a,i] = - G[m,i] * t[a,m] - (2 * U[m,n,i,e] - U[n,m,i,e]) * T[a,e,m,n] + (2 * w[a,m,i,e] - x[a,m,e,i]) * t[e,m] +
    (2 * T[a,e,i,m] - T[a,e,m,i]) * H[m,e] + (2 * r[a,m,e,f]  - r[a,m,f,e]) * tau[e,f,i,m]

  Z[a,b,i,j] = v[a,b,i,j] + Q[m,n,i,j]*tau[a,b,m,n] + y[a,b,e,f] * tau[e,f,i,j] + (1 + Perm[a/b, i/j]) * (
    r[e,j,a,b] * t[e,i] - P[i,j,m,b] * t[a,m] + F[a,e] * T[e,b,i,j] - G[m,i] * T[a,b,m,j] + 0.5 * W[b,m,j,e] *
    ( 2 * T[a,e,i,m] - T[a,e,m,i] ) - (0.5 + Perm[i/j]) * (X[b,m,e,j] * T[a,e,m,i]) )

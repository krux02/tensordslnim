import ../tensorDsl, ../tensormath, ../stopwatch, macros

var testProcs {.compileTime.} = newSeq[NimNode](0)
var exitStatus = 0

macro test(name: static[string]; arg: untyped): untyped =
  let namelit = newLit(name)
  let testSym = genSym(nskProc, name)

  let lineInfo = newLit arg.lineinfo
  let exitStatusSym = bindSym"exitStatus"

  result = quote do:
    proc `testSym`(): void =
      var stopwatch = newStopWatch(true)
      try:
        `arg`
        echo "[OK]   ", `nameLit`, ": ", stopwatch.timeSeconds, "s"
      except AssertionError:
        let exception = getCurrentException()
        echo "[FAIL] ", `nameLit`, ": assertion error: ", exception.msg, " time: ", stopwatch.timeSeconds, "s"
        echo exception.getStackTrace
        echo `lineinfo`
        `exitStatusSym` = 1

  testProcs.add testSym

template testDisabled(name: static[string]; arg: untyped): untyped =
  echo "[disabled] ", name

macro callKnownTests(): untyped =
  result = newStmtList()
  for sym in testProcs:
    result.add newCall(sym)
  result.add newCall(bindSym"quit", bindSym"exitStatus")

################################################################################
################################################################################
################################################################################

test("fill from array"):
  var table: array[0..5, array[0..3, float]] = [
    [0.000000,6.000000,12.000000,18.000000],
    [2.000000,8.000000,14.000000,20.000000],
    [4.000000,10.000000,16.000000,22.000000],
    [1.000000,7.000000,13.000000,19.000000],
    [3.000000,9.000000,15.000000,21.000000],
    [5.000000,11.000000,17.000000,23.000000]
  ]

  var theTensor : tensorType(2,3,4)

  theTensor.fillFromArray(table)

  doAssert theTensor.data == [
    0.000000,  6.000000, 12.000000, 18.000000,
    2.000000,  8.000000, 14.000000, 20.000000,
    4.000000, 10.000000, 16.000000, 22.000000,
    1.000000,  7.000000, 13.000000, 19.000000,
    3.000000,  9.000000, 15.000000, 21.000000,
    5.000000, 11.000000, 17.000000, 23.000000
  ]

test("tensormath"):
  var firstTensor: tensorType(2,3,4)

  fillIota(firstTensor)

  let tensorTransposedA = transpose(firstTensor, permutation(1,2,0), 1, 0)
  let tensorTransposedB = transpose(firstTensor, permutation(2,0,1), 1, 0)

  doAssert tensorTransposedA.data == [
    0.0,2.0,4.0,
    6.0,8.0,10.0,
    12.0,14.0,16.0,
    18.0,20.0,22.0,

    1.0,3.0,5.0,
    7.0,9.0,11.0,
    13.0,15.0,17.0,
    19.0,21.0,23.0
  ]

  var smallTensor : tensorType(2,2)

  smallTensor.data = [1.0'f64, 1, 1, 1]

  let tensorA = smallTensor * 4.0
  doAssert tensorA.data == [4.0, 4,4,4]
  let tensorB = smallTensor / 4.0
  doAssert tensorB.data == [0.25, 0.25, 0.25, 0.25]
  let tensorC = smallTensor + 4.0
  doAssert tensorC.data == [5.0, 5, 5, 5]
  let tensorD = smallTensor - 4.0
  doAssert tensorD.data == [-3.0, -3, -3, -3]

  doAssert firstTensor.Dim == dimensions(2,3,4)
  doAssert tensorTransposedA.Dim == dimensions(3,4,2)

  var tensorContracted = contract(firstTensor, tensorTransposedA, contraction([1,0]))

  doAssert tensorContracted.data == [
    20.0,26.0,
    56.0,62.0,
    92.0,98.0,
    128.0,134.0,

    56.0,80.0,
    200.0,224.0,
    344.0,368.0,
    488.0,512.0,

    92.0,134.0,
    344.0,386.0,
    596.0,638.0,
    848.0,890.0,

    128.0,188.0,
    488.0,548.0,
    848.0,908.0,
    1208.0,1268.0,


    26.0,35.0,
    80.0,89.0,
    134.0,143.0,
    188.0,197.0,

    62.0,89.0,
    224.0,251.0,
    386.0,413.0,
    548.0,575.0,

    98.0,143.0,
    368.0,413.0,
    638.0,683.0,
    908.0,953.0,

    134.0,197.0,
    512.0,575.0,
    890.0,953.0,
    1268.0,1331.0
  ]

  var mat1 = newTensor(2,3)
  var mat2 = newTensor(3,2)

  fillIota(mat1)
  fillIota(mat2)

  var mat3 = contract(mat1, mat2, contraction([1,0]))
  var mat4 = contract(mat1, mat2, contraction([0,1]))

  doAssert mat3.data == [10.0, 13, 28, 40]
  doAssert mat4.data == [3.0,9.0,15.0,  4.0,14.0,24.0, 5.0,19.0,33.0]

################################################################################
################################################################################
################################################################################

test("reuse expressions"):
  var tensorA : tensorType(4,7)
  var tensorB : tensorType(2,4)
  var tensorC : tensorType(7,2)

  tensorDsl:
    # debug
    export tensorX[j,k] := tensorA[i,j] * tensorB[k,i]
    export tensorY[b,c] := tensorC[b,c] + tensorA[a,b] * tensorB[c,a]

  # TODO make sure the expression is acutally reused !!! currently not tested at all

test("simple transpose"):

  var tensorA : tensorType(4,7)

  fillIota(tensorA)

  tensorDsl:
    # debug
    X[i,j] := tensorA[i,j]
    export tensorY[j,i] := tensorA[j,i]

  doAssert tensorY == tensorA


test("copy"):
  var tensorB : tensorType(2,3,4)
  fillIota(tensorB)
  tensorDsl:
    # debug
    export tensorC[i,j,k] := tensorB[i,j,k]
  doAssert tensorB == tensorC

testDisabled("minimal"):
  # this can be compiled into a single contraction, but it has never
  # been implemented to be this smart.

  proc foo(w:tensorType(2,2,2,3), x:tensorType(2,2,2,3)) : auto {.exportc.} =
    tensorDsl:
      export W[c,d,e,f] := 2.0 * w[c,k1,e,k2] * x[k1,d,f,k2]

    return W

  macro checkfoo(arg: typed): untyped =
    proc countContractionCalls(arg: NimNode): int =
      if arg.eqIdent("contraction"):
        result += 1
      for child in arg:
        result += countContractionCalls(child)

    let numCalls = countContractionCalls(arg.symbol.getImpl)
    if numCalls > 1:
      let message = newLit("numCalls should be 1, it is: " & $numCalls)
      result = quote do:
        assert false, `message`

  checkfoo(foo)

test("transpose"):
  var tensor1 : tensorType(2,3,4)
  fillIota(tensor1)
  tensorDsl:
    # debug
    export tensor0[i,j,k] := tensor1[k,i,j]
  var tensorX = transpose(tensor1, permutation(1, 2, 0), 1.0, 0.0)
  doAssert tensor0 == tensorX

test("contraction"):
  var
    mat1 : tensorType(2,3)
    mat2 : tensorType(3,4)
    mat3 : tensorType(4,2)

  fillIota(mat1)
  fillIota(mat2)
  fillIota(mat3)

  tensordsl:
    # debug
    export mat0[a,d] := mat1[a,b] * mat2[b,c] * mat3[c, d]

  let matX =
    mat1.contract(mat2, contraction([1, 0])).contract(mat3, contraction([1, 0]))

  doAssert mat0 == matX

test("add"):
  var tensor3 : tensorType(3,3,3)
  fillIota(tensor3)

  var a1 = transpose(tensor3, permutation(2,0,1))
  var a2 = transpose(tensor3, permutation(2,1,0))
  var a3 = transpose(tensor3, permutation(1,2,0))

  var addResultExpectedA = sum(a1,a2,a3)

  var b2   = transpose(tensor3, permutation(1,0,2))
  var b3   = transpose(tensor3, permutation(2,0,1))
  var bsum = sum(tensor3, b2, b3)

  var addResultExpectedB = transpose(bsum, permutation(2,0,1))

  doAssert addResultExpectedA == addResultExpectedB

  const permutationsB = [permutation(2,0,1), permutation(2,1,0), permutation(1,2,0)]

  var addResult : tensorType(3,3,3)
  addResult = sumTranspose(tensor3, permutationsB)

  doAssert addResult == addResultExpectedB

  const permutationsA = [
    permutation(0,1,2),
    permutation(1,0,2),
    permutation(2,0,1)
  ]

  var tmp: tensorType(3,3,3) = sumTranspose(tensor3, permutationsA)
  doAssert tmp == bsum


  tensordsl:
    # debug
    export addResult2[i,j,k] := tensor3[j,k,i] + tensor3[k,j,i] + tensor3[k,i,j]

  doAssert addResult2 == addResultExpectedA

  var scaleExpected : tensorType(3,3,3)
  for i,x in addResult.data:
    scaleExpected.data[i] = 0.5 * x

  tensordsl:
    # debug
    export addResult3[i,j,k] := 0.5 * addResult[i,j,k]

  doAssert addResult3 == scaleExpected

  when defined(UsePerm):
    var tensor4: tensorType(4,4,2,2)
    fillIota(tensor4)

    var permExpected: tensorType(4,4,2,2) = sum(tensor4, transpose(tensor4, permutation(0,1,3,2)))
    # var permResult: tensorType(4,4,2,2)

    tensordsl:
      # debug
      export permResult[m,n,i,j] := (1 + Perm[m/n, i/j]) * tensor4[m,n,i,j]

    doAssert permExpected == permResult

test("sumTransposeLoopCode"):
  var tensorA : tensorType(2,2,2)
  #var tensorB : tensorType(2,2,2)

  tensorDsl:
    # debug
    tensorB[i,j,k] := tensorA[i,j,k] + tensorA[j,i,k] + tensorA[k,j,i] + tensorA[j,k,i]


callKnownTests()

import ../tensormath.nim

var firstTensor: tensorType(2,3,4)
fillIota(firstTensor)

let tensorTransposedA = transpose(firstTensor, permutation(1,2,0), 1, 0)
doAssert tensorTransposedA.Dim == dimensions(3,4,2)

for i in 0 ..< firstTensor.Dim[0]:
  for j in 0 ..< firstTensor.Dim[1]:
    for k in 0 ..< firstTensor.Dim[2]:
      doAssert tensorTransposedA[j,k,i] == firstTensor[i,j,k]

for i in 0 ..< tensorTransposedA.Dim[0]:
  for j in 0 ..< tensorTransposedA.Dim[1]:
    for k in 0 ..< tensorTransposedA.Dim[2]:
      doAssert tensorTransposedA[i,j,k] == firstTensor[k,i,j]

let tensorTransposedB = transpose(firstTensor, permutation(2,0,1), 1, 0)
doAssert tensorTransposedB.Dim == dimensions(4,2,3)

for i in 0 ..< firstTensor.Dim[0]:
  for j in 0 ..< firstTensor.Dim[1]:
    for k in 0 ..< firstTensor.Dim[2]:
      doAssert tensorTransposedB[k,i,j] == firstTensor[i,j,k]

for i in 0 ..< tensorTransposedB.Dim[0]:
  for j in 0 ..< tensorTransposedB.Dim[1]:
    for k in 0 ..< tensorTransposedB.Dim[2]:
      doAssert tensorTransposedB[i,j,k] == firstTensor[j,k,i]

import ../tensormath.nim

import algorithm

# permutations

const
  p0 = permutation(0,1,2)
  p1 = permutation(0,2,1)
  p2 = permutation(1,0,2)
  p3 = permutation(1,2,0)
  p4 = permutation(2,0,1)
  p5 = permutation(2,1,0)

# test add
var tensor2x2x2: tensorType(2,2,2)
fillIota(tensor2x2x2)

let t0 = transpose(tensor2x2x2, p0)
let t1 = transpose(tensor2x2x2, p1)
var t2 = transpose(tensor2x2x2, p2)
let t3 = transpose(tensor2x2x2, p3)
let t4 = transpose(tensor2x2x2, p4)
let t5 = transpose(tensor2x2x2, p5)

let st0 = sumTranspose(tensor2x2x2, [p0])
let st1 = sumTranspose(tensor2x2x2, [p1])
var st2 = sumTranspose(tensor2x2x2, [p2])
let st3 = sumTranspose(tensor2x2x2, [p3])
let st4 = sumTranspose(tensor2x2x2, [p4])
let st5 = sumTranspose(tensor2x2x2, [p5])

doAssert t0 == st0
doAssert t1 == st1
doAssert t2 == st2
doAssert t3 == st3
doAssert t4 == st4
doAssert t5 == st5

# test add
var tensor3 : tensorType(3,3,3)
fillIota(tensor3)

block:
  let expected = sum(t4,t5,t3)
  let value = sumTranspose(tensor2x2x2, [p4, p5, p3])
  doAssert value == expected

block:
  let expected = sum(t2,t1,t0)
  let value = sumTranspose(tensor2x2x2, [p2, p1, p0])
  doAssert value == expected

block:
  let expected = sum(t2,t4,t3)
  let value = sumTranspose(tensor2x2x2, [p2, p4, p3])
  doAssert value == expected


block:
  let expected = tensor2x2x2 * 4
  let value = sumTranspose(tensor2x2x2, [p0, p0, p0, p0])
  doAssert value == expected

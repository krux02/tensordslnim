import moremacros, tensormath, sequtils, myutils, exportLatex

export tensormath
export namedEcho

const UsePerm = false

macro debugMacro(arg: typed): untyped =
  echo "<debugMacro>"
  echo arg.repr
  echo "</debugMacro>"
  return arg

proc isOperator(node:NimNode, name: string): bool =
  if node.kind == nnkIdent:
    node == ident(name)
  elif node.kind == nnkSym:
    $node.symbol == name
  else:
    false

proc sumArgs(arg: NimNode): seq[NimNode] =
  if arg.kind == nnkInfix and arg[0].isOperator("+"):
    result = arg[1].sumArgs & arg[2].sumArgs
  else:
    result = @[arg]

proc contractArgs(arg: NimNode): seq[NimNode] =
  if arg.kind == nnkInfix and arg[0].isOperator("*"):
    result = arg[1].contractArgs & arg[2].contractArgs
  else:
    result = @[arg]

proc matchBracketExpr(arg: NimNode): tuple[match: NimNode, stop: bool] =
  if arg.kind == nnkBracketExpr and arg[0].repr != "Perm":
    result.stop = true
    result.match = arg

var theScalar : Scalar = 0
proc ta(t: Tensor; indices: varargs[int]): var Scalar =
  ## dummy function call to pass to tensordslInner
  theScalar

proc isLeafNode(arg: NimNode): bool {.compileTime.} =
  arg.len == 0

proc callToInfix(arg: NimNode): NimNode {.compileTime.} =
  ## reverse to ``joinInfixToCall``
  if arg.isLeafNode or arg.kind == nnkBracketExpr:
    result = arg
  elif arg.kind == nnkCall and (arg[0].repr == "*" or arg[0].repr == "+"):
    result = nnkInfix.newTree(arg[0], callToInfix(arg[1]), callToInfix(arg[2]))
    for i in 3 ..< len(arg):
      result = nnkInfix.newTree(result, callToInfix(arg[i]))
  elif arg.kind == nnkAsgn:
    result = nnkAsgn.newTree(
      arg[0],
      callToInfix(arg[1])
    )
  elif arg.kind == nnkStmtList:
    result = newStmtList()
    for node in arg:
      result.add callToInfix(node)
  else:
    error "incompatible tree for callToInfix: " & $arg.kind, arg

proc substituteTensorAccess(arg: NimNode): NimNode {.compileTime.} =
  ## every time there is a bracket expression replace it with a call to ``ta``
  if arg.isLeafNode:
    result = arg
  elif arg.kind == nnkBracketExpr and arg[0] != ident"Perm":
    result = newCall(bindSym"ta")
  else:
    result = arg.kind.newTree

  for x in arg:
    result.add substituteTensorAccess(x)

# proc isOperatorIdent(arg: NimNode): bool {.compileTime.} =
#   # tests if the node is a basic math operator identifier(+-*/)
#   if arg.kind == nnkIdent:
#     let str = $arg.ident
#     result = str == "*" or str == "+" or str == "-" or str == "/"
#   else:
#     result = false

proc warnPerm(arg: NimNode): bool {.compileTime.} =
  if arg.kind == nnkBracketExpr and arg[0] == ident"Perm":
    result = true
    warning "Perm operator is a very big hack"

  for node in arg:
    result = result or warnPerm(node)

proc substitude(arg: NimNode; pars: openarray[tuple[a,b: NimNode]]): NimNode =
  if arg.kind == nnkBracketExpr:
    result = nnkBracketExpr.newTree()
    for n in arg:
      var mappedIdent = n
      for p in pars:
        if p.a == n:
          mappedIdent = p.b
          break
        elif p.b == n:
          mappedIdent = p.a
          break

      result.add mappedIdent

  elif arg.isLeafNode:
    result = arg
  else:
    result = arg.kind.newTree
    for n in arg:
      result.add(arg.substitude(pars))

import tables

when UsePerm:
  proc recResolvePerm(arg: NimNode): NimNode {.compileTime.} =
    # TODO!!! test this function and finish it!!!
    if isLeafNode(arg) or arg.kind == nnkBracketExpr:
      return arg

    elif (var v = isNodeProPermTransform(arg); v.ok):
      var
        preFactors  = newSeq[NimNode]()
        postFactors = newSeq[NimNode]()

      for i in 0 .. < v.prodIdx:
        preFactors.add arg[i]
      for i in v.prodIdx+1 .. < len(arg):
        postFactors.add arg[i]

      let sum = arg[v.prodIdx]
      let newSum = newCall(sum.head)
      for summand in sum:
        let newProduct = newCall(arg.head, summand)
        newProduct.addAll postFactors
        newSum.add newProduct

      if preFactors.len > 0:
        result = newCall(arg.head)
        result.addAll preFactors
        result.add newSum
      else:
        result = newSum

  proc resolvePerm(arg: NimNode): NimNode {.compileTime.} =
    let infix = joinInfixToCall(arg)
    result = callToInfix(a)

import tensorDslInner

macro cleanupMacro(arg: typed): untyped =
  ## removes the unused var section
  result = arg
  result[1][1].del(0)

macro tensordsl*(inputAst: untyped): untyped =
  #let tensorIdents =  recAstCollect(ast, matchTensorNames)
  var debug : bool = false

  var ast = inputAst
  #if warnPerm(ast):
  #  ast = resolvePerm(ast)
  var varSection = newVarSection()

  # find all identifiers that are used for
  # for example:
  # var i,j,k: int
  var identifiers = newSeq[NimNode]()
  for bracketExpr in recAstCollect(ast, matchBracketExpr):
    for i in 1 ..< bracketExpr.len:
      identifiers.add bracketExpr[i]

  let indexVarSection = newVarSection newIdentDefs(
    deduplicate(identifiers),
    ident"int", newEmptyNode()
  )

  var assignments = newStmtList()
  var exportAll = false
  var exportAssignment = newSeq[bool](0)

  for stmtNode in ast:
    if stmtNode == ident"debug":
      debug = true
    elif stmtNode == ident"exportLatex":
      exportLatex(inputAst)
    elif stmtNode == ident"exportAll":
      exportAll = true
    elif stmtNode.kind == nnkAsgn:
      error("pure assignments currently not supported, use := instead", stmtNode)
    else:
      var asgn: NimNode
      var exportSym: bool = false

      if stmtNode.kind == nnkInfix and stmtNode[0] == ident":=":
        asgn = stmtNode
      elif stmtNode.kind == nnkExportStmt and stmtNode[0].kind == nnkInfix  and stmtNode[0][0] == ident":=":
        asgn = stmtNode[0]
        exportSym = true
      else:
        error("unexpected statement", stmtNode)

      var resultTypeExpr = newCall(bindSym"tensorType")

      for i in 1 ..< len(asgn[1]):
        let forVar = asgn[1][i]

        var indexUsages = newSeq[NimNode](0)

        proc collectIndexUsage(arg: NimNode): void =
          if arg.kind == nnkBracketExpr:
            if arg[0] == ident("Perm"):
              error("Perm is reserved for permutation, but currently not supported", arg[0])

            let idx = arg.indexOf(forVar)
            if idx > 0:
              indexUsages.add newDotExpr(arg[0], ident"Dim").newBracketExpr(newLit(idx-1))
          else:
            for child in arg:
              collectIndexUsage(arg)


        proc matchIndexUsage(arg: NimNode): tuple[match: NimNode; stop: bool] =
          if arg.kind == nnkBracketExpr:
            if arg[0] == ident("Perm"):
              error("Perm is reserved for permutation, but currently not supported", arg[0])

            result.stop = true
            let idx = arg.indexOf(forVar)
            if idx > 0:
              result.match =
                newDotExpr(arg[0], ident"Dim").newBracketExpr(newLit(idx-1))

        let matches = recAstCollect(asgn[2], matchIndexUsage)
        resultTypeExpr.add matches[0]

      asgn[1].expectKind nnkBracketExpr

      let identDef = newIdentDefs(
        asgn[1][0], resultTypeExpr, newEmptyNode()
      )

      exportAssignment.add exportSym
      varSection.add identDef

      assignments.add newAsgn(
        substituteTensorAccess(asgn[1]),
        substituteTensorAccess(asgn[2])
      )

  if exportAll:
    for exportFlag in exportAssignment.mitems:
      exportFlag = true

  assert exportAssignment.len == assignments.len
  assert assignments.len == varSection.len

  ## substitude unexported symbols:

  var substitutionTable = newTable[NimNode, NimNode]()

  for i, exprot in exportAssignment:
    if not exprot:
      let ident = varSection[i][0]
      let subst = genSym(nskVar, ident.repr)

      substitutionTable[ident] = subst

  proc recSubstitudeSymbol(arg: NimNode): NimNode =
    if arg.kind == nnkIdent and substitutionTable.hasKey(arg):
      return substitutionTable[arg]
    else:
      for i in 0 ..< arg.len:
        arg[i] = recSubstitudeSymbol(arg[i])

      return arg

  varSection = recSubstitudeSymbol(varSection)
  assignments = recSubstitudeSymbol(assignments)

  ## generate call to the inner tensordsl
  result = newStmtList(
      varSection,
      nnkBlockStmt.newTree(
        newEmptyNode(),
        newStmtList(
          indexVarSection,
          newCall(bindSym"tensorDslInner", newLit(debug), newArrayLit(exportAssignment), assignments)
        )
      )
    )

  if debug:
    result = newCall(bindSym"debugMacro", result)
  else:
    result = newCall(bindSym"cleanupMacro", result)
